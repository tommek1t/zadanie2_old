import { Component, OnInit, Input } from '@angular/core';
import {Doctor} from './doctor';
import {DoctorService} from './doctor.service';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'plan-visit',
    templateUrl: 'app/html/plan-visit.component.html',
    styleUrls: ['app/css/detail.component.css']
})
export class PlanVisitComponent implements OnInit {
    lista: any[] = [];
    sub: any;
    doctor: Doctor;
    docreservation: any[] = [];

    ngOnInit() {
        this.sub = this.actRoute.params.subscribe(params => {
            let id = params['id'];
            this.doctorService.getDoctor(id)
                .then(doctor => this.doctor = doctor)
                .then(() =>
                    this.doctorService.getPlan(this.doctor._id)
                        .then(v => this.lista = v))
                .then(() => this.doctorService.getReservation(this.doctor._id)
                    .then(r => this.docreservation = r))
                .then(() => this.lista
                    .sort(function (a, b) {
                        /*if (a.date < b.date) return -1;
                        else return 1;*/

                        if (a.date == b.date) {
                            return (a.hour1 < b.hour1) ? -1 : (a.hour1 > b.hour1) ? 1 : 0;
                        }
                        else {
                            return (a.date < b.date) ? -1 : 1;
                        }
                    }));
        });
    }

    constructor(private doctorService: DoctorService, private route: Router, private actRoute: ActivatedRoute) { }


    private isReservated(poz: any) {
        let ok = true;
        for (let i = 0; i < this.docreservation.length; i++) {
            if (this.docreservation[i].Visit._id === poz._id)
                ok = false;
        }
        return ok;
    }

    editPoz(poz: any) {
        if (this.isReservated(poz)) {
            let link = ['/editVisit', poz._id, this.doctor._id];
            this.route.navigate(link);
        }
        else alert("Już dokonano rezerwacji na wizytę. Edycja zablokowana.");
    }

    delPoz(poz: any) {
        if (this.isReservated(poz)) {
            if (confirm("Czy na pewno chcesz usunac ta pozycje?")) {
                this.doctorService.deleteVisit(poz._id)
                    .then(() => window.location.reload());
            }
        }
        else alert("Już dokonano rezerwacji na wizytę. Usunięcie wizyty zablokowane.");
    }

    addNewVisit() {
        this.route.navigate(['/addVisit', this.doctor._id]);
    }

}