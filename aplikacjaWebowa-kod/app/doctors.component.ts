import { Component, OnInit } from '@angular/core';
import { Doctor } from './doctor';
import { DoctorDetailComponent } from './doctor-detail.component';
import {DoctorService} from './doctor.service';
import {Router} from '@angular/router';

@Component({
  selector: 'list-doctors',
  templateUrl: 'app/html/doctors.component.html',
  styleUrls: ['app/css/doctors.component.css'],
  directives: [DoctorDetailComponent]
})


export class DoctorsComponent implements OnInit {
  title = 'Lista Lekarzy';
  selectedDoctor: Doctor;
  doctors: Doctor[];

  ngOnInit() {
    this.getDoctors();
  }
  onSelect(doctor: Doctor) {
    this.selectedDoctor = doctor;
    let link = ['/detail', this.selectedDoctor._id.valueOf()];
    this.router.navigate(link);
  }

  constructor(private doctorService: DoctorService, private router: Router) {
  }
  getDoctors() {
    this.doctorService.getDoctors()
      .then(doctors => this.doctors = doctors).then(() => this.doctors
        .sort(function (a, b) {
          if (a.name < b.name) return -1;
          else return 1;
        }));
  }

  addDoctor() {
    this.router.navigate(['/newDoc']);
  }

  close(savedDoctor: Doctor) {
    if (savedDoctor) { this.getDoctors(); }
  }

  deleteDoc(doctor: Doctor, event: any) {
   var existVis=true;
    if (confirm("Czy na pewno chcesz usunac tego lekarza?")) {
      this.doctorService.getPlan(doctor._id).then(plan => existVis = (plan.length === 0)).then(() => {
        if (existVis) {
          event.stopPropagation();
          this.doctorService
            .delete(doctor)
            .then(res => {
              this.doctors = this.doctors.filter(d => d !== doctor);
            });
        }
        else
          alert("Ten lekarz ma ustalone wizyty!");
      })
    }
  }
}




