import { Component, EventEmitter, Input, OnInit, OnDestroy, Output } from '@angular/core';
import {Doctor} from './doctor';
import {ActivatedRoute} from '@angular/router';
import {DoctorService} from './doctor.service';
import {PlanVisitComponent} from './plan-visit.component';
import {Router} from '@angular/router';

@Component({
  selector: 'my-doctor-detail',
  templateUrl: 'app/html/doctor-detail.component.html',
  styleUrls: ['app/css/detail.component.css'],
  directives: [PlanVisitComponent]
})

export class DoctorDetailComponent implements OnInit, OnDestroy {
  @Input() doctor: Doctor;
  @Output() close = new EventEmitter();
  error: any;
  sub: any;
  navigated = false;
  newSpec: string = "";


  constructor(private doctorService: DoctorService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        let id = params['id'];
        this.doctorService.getDoctor(id).then(doctor => this.doctor = doctor);
        let link = ['/plan', params['id']];
        this.router.navigate(link);
      } else {
        this.doctor = new Doctor();
        this.doctor.specjalizacja = [];
      }
    });


  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  goBack(savedDoctor: Doctor = null) {
    this.close.emit(savedDoctor);//emit to notify
    if (this.navigated) { this.router.navigate(['/doctors']); }
  }

  save() {
    this.doctorService
      .save(this.doctor)
      .then(()=>this.router.navigate(['/doctors']))
      .catch(error => this.error = error);
  }

  addSpec() {
    this.doctor.specjalizacja.push(this.newSpec);
    this.newSpec = "";
  }

  delAllSpec() {
    this.doctor.specjalizacja = [];
  }

  delSpec(spec) {
    let index = this.doctor.specjalizacja.indexOf(spec);
    this.doctor.specjalizacja.splice(index, 1);
  }

  back(){
        this.router.navigate(['/doctors']);
    }

}