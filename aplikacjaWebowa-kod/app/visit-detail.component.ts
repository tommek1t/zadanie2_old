import { Component, EventEmitter, Input, OnInit, OnDestroy, Output } from '@angular/core';
import {Doctor} from './doctor';
import {ActivatedRoute} from '@angular/router';
import {DoctorService} from './doctor.service';
import {PlanVisitComponent} from './plan-visit.component';
import {Router} from '@angular/router';

@Component({
    selector: 'my-visit-detail',
    templateUrl: 'app/html/visit-detail.component.html',
    styleUrls: ['app/css/detail.component.css'],
    directives: [PlanVisitComponent]
})

export class VisitDetailComponent implements OnInit, OnDestroy {
    visit: any;
    error: any;
    sub: any;
    doctor: Doctor;
    specjalizacja: string = "";


    constructor(private doctorService: DoctorService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] !== undefined) {
                let id = params['id'];
                this.doctorService.getVisit(id).then(v => this.visit = v[0]).then(visit=>this.specjalizacja=visit.doctor_specjalizacja);
            } else {
                this.visit = {
                    date: Date,
                    doctor_specjalizacja: "",
                    city: "",
                    hour1:"",
                    hour2:""
                };
            }
            if (params['iddoc'] !== undefined) {
                this.doctorService.getDoctor(params['iddoc']).then(d => this.doctor = d);
            }
        });

    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    save() {
        this.visit['doctor_specjalizacja'] = this.specjalizacja;
        this.visit['doctor_name'] = this.doctor.name;
        this.visit['id_doc'] = this.doctor._id;
        this.doctorService
            .saveVisit(this.visit)
            .then(doc => {
                this.router.navigate(['detail', this.doctor._id]);
            })
            .catch(error => this.error = error);
    }

    back(){
        this.router.navigate(['/detail',this.doctor._id]);
    }
}