export class User {
  id: number;
  login: string;
  passwd: string;
  name: string;
}