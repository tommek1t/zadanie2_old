import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import {DoctorService} from './doctor.service';
import {Page} from 'ui/page'
import timePicker = require("ui/time-picker");

@Component({
    selector: "visit",
    templateUrl: "visit-detail.component.html"
})

export class VisitDetailComponent implements OnInit {
    sub: any;
    visit: any;
    doctor: any;
    spec: number;
    timePicker1: any;
    timePicker2: any;

    constructor(private doctorService: DoctorService, private route: ActivatedRoute, private router: Router, private page: Page) { }


    ngOnInit() {
        this.doctor = this.doctorService.doctor;
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] !== undefined) {
                let id = params['id'];
                this.doctorService.getVisit(id)
                    .then(v => this.visit = v[0])
                    .then(() => this.setTime())
                    .then(() => this.spec = this.doctor.specjalizacja.indexOf(this.visit.doc_specjalizacja))
            } else {
                this.visit = {
                    date: Date,
                    doctor_specjalizacja: "",
                    city: "",
                    hour1: "00:00",
                    hour2: "00:00"
                };
            };
        });
    }

    selectedIndexChanged(selected: any) {
        this.spec = selected.selectedIndex;
    }

    setTime() {
        this.timePicker1 = new timePicker.TimePicker();
        this.timePicker2 = new timePicker.TimePicker();
        let time1 = this.visit.hour1.split(":");
        let time2 = this.visit.hour2.split(":");
        console.log(this.visit.hour1);

        if (this.timePicker1.android) {
            this.timePicker1.android.setIs24HourView(true);
            this.timePicker2.android.setIs24HourView(true);
        }
        else if (this.timePicker1.ios) {
            this.timePicker1.ios.locale = this.timePicker1.alloc().initWithLocaleIdentifier("PL");
            this.timePicker2.ios.locale = this.timePicker2.alloc().initWithLocaleIdentifier("PL");
        }
        this.timePicker1.minute = parseInt(time1[1]);
        this.timePicker1.hour = parseInt(time1[0]);
        this.timePicker2.minute = parseInt(time2[1]);
        this.timePicker2.hour = parseInt(time2[0]);
    }

    save() {
        if (!this.visit._id)//aby nie wysylac null'a przy braku zmiany
            this.visit.doctor_specjalizacja = this.doctor.specjalizacja[this.spec];
        this.visit.doctor_name=this.doctor.name;
        this.visit.date = this.visit.date.toISOString().substring(0, 10);
        this.doctorService.saveVisit(this.visit).then(() => this.router.navigate(['/']));
    }
}