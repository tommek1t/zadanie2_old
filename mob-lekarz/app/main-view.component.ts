import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router';
import {DoctorService} from './doctor.service';
var applicationSettings = require("application-settings");
var dialogs = require("ui/dialogs");

@Component({
    selector: "main",
    templateUrl: "main-view.component.html",
    styleUrls:["app.css"]
})

export class MainViewComponent implements OnInit {
    list: string[] = [];
    visits:any[]=[];

    constructor(private router: Router, private doctorService: DoctorService) { }

    ngOnInit() {
        if (applicationSettings.getString("id") === undefined)
            this.router.navigate(['/login']);
        else
            this.doctorService.getMyVisits()
                .then(obiekty => {
                    obiekty.forEach(el => {
                        this.list.push(el.date + ", od " + el.hour1 + " do " + el.hour2 + "\n ("
                            + el.city + ", " + el.doctor_specjalizacja + ")");
                        this.visits.push(el);
                    });
                });  
    }

    addNew() {
        this.router.navigate(['/visit-detail']);
    }

     public listViewItemTap(position) {
       this.router.navigate(['/visit-readOnly',this.visits[position.index]._id]);
    }

    public listViewItemLongPress(position){
         dialogs.confirm({
            title: this.list[position.index],
            message: "Edytować czy Usunać?",
            okButtonText: "Edytuj",
            cancelButtonText: "Usuń",
        }).then(result => {
            //toDO: sprawdz, czy juz dokonano rtezerwacji
            //potem { ......i ponizsze tutaj....}
            if (result) {//edit
                console.log(position.index);
                this.router.navigate(['/visit-detail',this.visits[position.index]._id]);
            }
            else{//del
                
                //usun
            }
        });
    }
}