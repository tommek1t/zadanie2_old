import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
var applicationSettings = require("application-settings");
import {DoctorService} from './doctor.service';

@Component({
    selector: "login",
    templateUrl: "login.component.html",
})
export class LoginComponent implements OnInit {
    id:string;

    constructor(private router: Router,private doctorService:DoctorService) {
    }

tryLogIn(){
    //toDO
        applicationSettings.setString("id", "5791cc64da5bd44c100bb49a");//
        this.id=applicationSettings.getString("id");
        console.log(this.id);
        this.doctorService.setDoctor(this.id).then(()=>this.router.navigate(['/']));
}

    ngOnInit() {
      let iduser = applicationSettings.getString("id", "none");
        if (iduser !== "none") {
            this.id = iduser;
            this.tryLogIn();
        }
    }
}
