import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import {DoctorService} from './doctor.service';
import timePicker = require("ui/time-picker");

@Component({
    selector: "visit",
    templateUrl: "visit-read-only.component.html"
})

export class VisitReadComponent implements OnInit {
    sub: any;
    visit: any;

    constructor(private doctorService: DoctorService, private route: ActivatedRoute, private router: Router) { }


    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
                let id = params['id'];
                this.doctorService.getVisit(id)
                    .then(v => this.visit = v[0]);
        })
    }

    back() {
        this.router.navigate(['/']);
    }
}