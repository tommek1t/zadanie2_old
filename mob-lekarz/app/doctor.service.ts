import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/add/operator/toPromise';
var applicationSettings = require("application-settings");

@Injectable()
export class DoctorService {
    serverURL = "http://10.132.226.163:1199/admin/";
    doctor: any;

    constructor(private http: Http) { }

    getMyVisits() {
        let id = applicationSettings.getString("id");
        console.log(id);

        return this.http.get(this.serverURL + "visits/" + id)
            .map(response => response.json())
            .toPromise();
    }

    getVisit(vis: number) {
        return this.http.get(this.serverURL + "visit/" + vis).map(response => response.json()).toPromise();
    }

    setDoctor(iddoc: string) {
        return this.http.get(this.serverURL + "doctor/" + iddoc)
            .map(response => response.json())
            .toPromise().then(data => this.doctor = data);
    }

    saveVisit(visit: any) {
        if (visit._id)
            return this.http.put(this.serverURL + "visit", visit).toPromise();
        else
            return this.http.post(this.serverURL + "visit", visit).toPromise();
    }

    deleteVisit(vis:number){

    }

}