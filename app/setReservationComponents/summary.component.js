"use strict";
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var user_service_1 = require("../user.service");
var SummaryComponent = (function () {
    function SummaryComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.reason = " ";
    }
    SummaryComponent.prototype.cancel = function () {
        this.router.navigate(["/spec-okres"]);
    };
    SummaryComponent.prototype.submit = function () {
        this.userService.pushReservation(this.visit, this.reason).subscribe(function (status) {
            console.log("status", status);
            alert("Dokonano rezerwacji");
        }, function (err) { return console.error(err); });
        this.router.navigate(["/menu"]);
    };
    SummaryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getVisit().then(function (v) { return _this.visit = v; }).then(function () {
            _this.lekarz = _this.visit.doctor_specjalizacja + " " + _this.visit.doctor_name;
            _this.userService.getUserName().then(function (data) { return _this.name = data; });
        });
    };
    SummaryComponent = __decorate([
        core_1.Component({
            selector: "summary-visit",
            templateUrl: "html/summary.component.html",
            styleUrls: ["css/forms.css"]
        }), 
        __metadata('design:paramtypes', [router_1.Router, user_service_1.UserService])
    ], SummaryComponent);
    return SummaryComponent;
}());
exports.SummaryComponent = SummaryComponent;
//# sourceMappingURL=summary.component.js.map