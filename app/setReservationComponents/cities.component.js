"use strict";
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var user_service_1 = require("../user.service");
var CitiesComponent = (function () {
    function CitiesComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.wybor = [];
        this.ladowanie = true;
    }
    CitiesComponent.prototype.next = function () {
        var _this = this;
        var cities = [];
        Promise.resolve().then(function () { return _this.wybor.forEach(function (w) { return cities.push(_this.lista[w]); }); })
            .then(function () { return _this.userService.setCities(cities); })
            .then(function () { return _this.router.navigate(["/visits"]); });
    };
    CitiesComponent.prototype.back = function () {
        this.router.navigate(["/spec-okres"]);
    };
    CitiesComponent.prototype.jumpIt = function () {
        var _this = this;
        Promise.resolve().then(function () { return _this.userService.setCities(_this.lista); })
            .then(function () { return _this.router.navigate(["/visits"]); });
    };
    CitiesComponent.prototype.clear = function () {
        this.wybor = [];
    };
    CitiesComponent.prototype.listViewItemTap = function (args) {
        console.log("tapped", args.index);
        if (this.wybor.indexOf(args.index) === -1)
            this.wybor.push(args.index);
        //item.color
    };
    CitiesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getActualCities().then(function (cities) { return _this.lista = cities; }).then(function () { return _this.ladowanie = false; });
    };
    CitiesComponent = __decorate([
        core_1.Component({
            selector: "cities",
            templateUrl: "html/cities.component.html",
            styleUrls: ["css/forms.css"]
        }), 
        __metadata('design:paramtypes', [router_1.Router, user_service_1.UserService])
    ], CitiesComponent);
    return CitiesComponent;
}());
exports.CitiesComponent = CitiesComponent;
//# sourceMappingURL=cities.component.js.map