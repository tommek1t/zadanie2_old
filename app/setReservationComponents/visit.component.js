"use strict";
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var user_service_1 = require("../user.service");
var VisitComponent = (function () {
    function VisitComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.lista = [];
    }
    VisitComponent.prototype.listViewItemTap = function (pozycja) {
        var _this = this;
        Promise.resolve().then(function () { return _this.userService.setChooseVisit(pozycja.index); })
            .then(function () { return _this.router.navigate(["/summary"]); });
    };
    VisitComponent.prototype.back = function () {
        this.userService.cities = [];
        this.router.navigate(["/spec-okres"]);
    };
    VisitComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getSpecyficVisits().then(function (obiekty) {
            obiekty.forEach(function (el) {
                console.log(el);
                _this.lista.push(el.date + ", od " + el.hour1 + " do " + el.hour2 + "\n ("
                    + el.city + ", " + el.doctor_specjalizacja + ")");
            });
        });
    };
    VisitComponent = __decorate([
        core_1.Component({
            selector: "visit",
            templateUrl: "html/visit.component.html",
            styleUrls: ["css/forms.css"]
        }), 
        __metadata('design:paramtypes', [router_1.Router, user_service_1.UserService])
    ], VisitComponent);
    return VisitComponent;
}());
exports.VisitComponent = VisitComponent;
//# sourceMappingURL=visit.component.js.map