"use strict";
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var user_service_1 = require("../user.service");
var dialogs_1 = require("ui/dialogs");
var SpecOkresComponent = (function () {
    function SpecOkresComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.chceSpecjalizacje = true;
        this.chceDateDO = true;
    }
    SpecOkresComponent.prototype.selectedIndexChanged = function (picker) {
        this.specjalista = picker.selectedIndex;
    };
    SpecOkresComponent.prototype.next = function () {
        var _this = this;
        if (this.chceSpecjalizacje === false)
            this.specjalista = -1;
        if (this.chceDateDO === false)
            this.dataDO = new Date(2050, 12, 31, 23, 59);
        if (this.dataDO < this.dataOD)
            dialogs_1.alert("Data końcowa jest wcześniejsza niż poczatkowa!");
        else {
            Promise.resolve().then(function () { return _this.userService.setOkres(_this.dataOD, _this.dataDO); })
                .then(function () { return _this.userService.setSpec(_this.specjalista); })
                .then(function () { return _this.router.navigate(["/cities"]); });
        }
    };
    SpecOkresComponent.prototype.jumpIt = function (elem) {
        if (elem == 's') {
            this.chceSpecjalizacje = !this.chceSpecjalizacje;
        }
        if (elem == 'd2') {
            this.chceDateDO = !this.chceDateDO;
        }
    };
    SpecOkresComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getActualSpecialization().then(function (spec) { return _this.lista = spec; });
    };
    SpecOkresComponent = __decorate([
        core_1.Component({
            selector: "spec-okres",
            templateUrl: "html/spec-okres.component.html",
            styleUrls: ["css/forms.css"]
        }), 
        __metadata('design:paramtypes', [router_1.Router, user_service_1.UserService])
    ], SpecOkresComponent);
    return SpecOkresComponent;
}());
exports.SpecOkresComponent = SpecOkresComponent;
//# sourceMappingURL=spec-okres.component.js.map