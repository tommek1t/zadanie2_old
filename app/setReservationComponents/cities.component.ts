import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
import {UserService} from "../user.service";

@Component({
    selector: "cities",
    templateUrl: "html/cities.component.html",
    styleUrls: ["css/forms.css"]
})
export class CitiesComponent implements OnInit{
    lista: string[];
    wybor: number[] = [];
    ladowanie:boolean=true;

    public next() {
        let cities: string[] = [];
        Promise.resolve().then(()=>this.wybor.forEach(w => cities.push(this.lista[w])))
        .then(()=>this.userService.setCities(cities))
        .then(()=>this.router.navigate(["/visits"]));
    }

    public back() {
        this.router.navigate(["/spec-okres"]);
    }

    public jumpIt() {
        Promise.resolve().then(()=>this.userService.setCities(this.lista))
        .then(()=>this.router.navigate(["/visits"]));
    }

    public clear() {
        this.wybor = [];
    }

    listViewItemTap(args) {
        console.log("tapped", args.index);
        if (this.wybor.indexOf(args.index) === -1)
            this.wybor.push(args.index);
        //item.color
    }

    constructor(private router: Router, private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getActualCities().then(cities=>this.lista=cities).then(()=>this.ladowanie=false);
    }
}
