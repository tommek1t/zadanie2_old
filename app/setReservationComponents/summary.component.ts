import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
import {UserService} from "../user.service";
import {Visit} from '../visit';

@Component({
    selector: "summary-visit",
    templateUrl: "html/summary.component.html",
    styleUrls: ["css/forms.css"]
})
export class SummaryComponent implements OnInit{
    wybor: Object;
    lekarz: string;
    name: string; //user'a
    reason: string = " ";
    visit: Visit;

    public cancel() {
        this.router.navigate(["/spec-okres"]);
    }

    public submit() {
        this.userService.pushReservation(this.visit, this.reason).subscribe(
            status => {
                console.log("status", status);
                alert("Dokonano rezerwacji")
            },
        err=> console.error(err)
        );

        this.router.navigate(["/menu"]);
    }

    constructor(private router: Router, private userService: UserService) { }

    ngOnInit() {
        this.userService.getVisit().then(v => this.visit = v).then(() => {
            this.lekarz = this.visit.doctor_specjalizacja + " " + this.visit.doctor_name;
            this.userService.getUserName().then(data=>this.name=data);
        });
    }
}
