import {Component, OnInit,OnDestroy} from "@angular/core";
import {NS_ROUTER_DIRECTIVES} from "nativescript-angular/router";

@Component({
  selector: "main",
  directives: [NS_ROUTER_DIRECTIVES],
  template: "<page-router-outlet></page-router-outlet>",
  styleUrls: ["css/forms.css"]
})
export class AppComponent {

}